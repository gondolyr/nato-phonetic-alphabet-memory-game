//! NATO phonetic alphabet, code, and signals.
//!
//! This conforms to the ["20180111" specification](https://www.nato.int/nato_static_fl2014/assets/pdf/pdf_2018_01/20180111_nato-alphabet-sign-signal.pdf).

use std::fmt;

/// Semaphore hand positions from 0-7 with 0 representing the straight up, or 12 o'clock.
///
/// This implementation assumes the signaller is facing the receiver and the position is calculated
/// in a clockwise rotation.
///
/// Each number should be multiplied by 45 if using degrees.
///
/// Semaphore is a system in which a person sends information at a distance using hand-held
/// flags – depending on the position of the flags, the message will vary.
/// The signaller holds the flag in different positions that represent letters or numbers.
#[derive(Debug, Clone, PartialEq)]
pub struct SemaphorePositions {
    left: u8,
    right: u8,
}

pub trait NatoCode {
    /// Return the international Morse code representation.
    ///
    /// Morse code transmits text through on-off tones, light-flashes or clicks.
    /// It was widely used in the 1890s for early radio communication, before it was possible to
    /// transmit voice.
    ///
    /// `false` is short (`.`) and `true` is long (`-`).
    ///
    /// This may also be used for panel signalling.
    fn morse(&self) -> Vec<bool>;

    /// Panels are visual signals for sending simple messages to an aircraft.
    /// Using a limited code, ground forces can send messages to pilots, for example to request
    /// medical supplies.
    ///
    /// This is an alias for [`morse()`](Self::morse()).
    fn panel(&self) -> Vec<bool> {
        self.morse()
    }

    /// Return the phonetic pronounciation of the letter.
    ///
    /// The NATO alphabet became effective in 1956 and, a few years later, turned into the
    /// established universal phonetic alphabet for all military, civilian and amateur radio
    /// communications.
    fn phonetic(&self) -> &str;

    /// Return the hand positions from 0-7 with 0 representing the straight up, or 12 o'clock.
    ///
    /// This implementation assumes the signaller is facing the receiver and the position is
    /// calculated in a clockwise rotation.
    ///
    /// Each number should be multiplied by 45 if using degrees.
    ///
    /// Semaphore is a system in which a person sends information at a distance using hand-held
    /// flags – depending on the position of the flags, the message will vary.
    /// The signaller holds the flag in different positions that represent letters or numbers.
    fn semaphore(&self) -> SemaphorePositions;
}

/// Universal phonetic alphabet standardized by the NATO organization.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum NatoAlphabetLetter {
    /// (_al-fah_)
    ///
    /// Morse code: `.-`
    Alfa,
    /// (_brah-voh_)
    ///
    /// Morse code: `-...`
    Bravo,
    /// (_char-lee_)
    ///
    /// Morse code: `-.-.`
    Charlie,
    /// (_dell-tah_)
    ///
    /// Morse code: `-..`
    Delta,
    /// (_eck-oh_)
    ///
    /// Morse code: `.`
    Echo,
    /// (_foks-trot_)
    ///
    /// Morse code: `..-.`
    Foxtrot,
    /// (_golf_)
    ///
    /// Morse code: `--.`
    Golf,
    /// (_hoh-tel_)
    ///
    /// Morse code: `....`
    Hotel,
    /// (_in-dee-ah_)
    ///
    /// Morse code: `..`
    India,
    /// (_jew-lee-ett_)
    ///
    /// Morse code: `.---`
    Juliett,
    /// (_key-loh_)
    ///
    /// Morse code: `-.-`
    Kilo,
    /// (_lee-mah_)
    ///
    /// Morse code: `.-..`
    Lima,
    /// (_mike_)
    ///
    /// Morse code: `--`
    Mike,
    /// (_no-vem-ber_)
    ///
    /// Morse code: `-.`
    November,
    /// (_oss-cah_)
    ///
    /// Morse code: `---`
    Oscar,
    /// (_pah-pah_)
    ///
    /// Morse code: `.--.`
    Papa,
    /// (_keh-beck_)
    ///
    /// Morse code: `--.-`
    Quebec,
    /// (_row-me-oh_)
    ///
    /// Morse code: `.-.`
    Romeo,
    /// (_see-air-rah_)
    ///
    /// Morse code: `...`
    Sierra,
    /// (_tang-go_)
    ///
    /// Morse code: `-`
    Tango,
    /// (_you-nee-form_)
    ///
    /// Morse code: `..-`
    Uniform,
    /// (_vic-tah_)
    ///
    /// Morse code: `...-`
    Victor,
    /// (_wiss-key_)
    ///
    /// Morse code: `.--`
    Whiskey,
    /// (_ecks-ray_)
    ///
    /// Morse code: `-..-`
    Xray,
    /// (_yang-key_)
    ///
    /// Morse code: `-.--`
    Yankee,
    /// (_zoo-loo_)
    ///
    /// Morse code: `--..`
    Zulu,
}

impl NatoCode for NatoAlphabetLetter {
    fn morse(&self) -> Vec<bool> {
        match self {
            Self::Alfa => vec![false, true],
            Self::Bravo => vec![true, false, false, false],
            Self::Charlie => vec![true, false, true, false],
            Self::Delta => vec![true, false, false],
            Self::Echo => vec![false],
            Self::Foxtrot => vec![false, false, true, false],
            Self::Golf => vec![true, true, false],
            Self::Hotel => vec![false, false, false, false],
            Self::India => vec![false, false],
            Self::Juliett => vec![false, true, true, true],
            Self::Kilo => vec![true, false, true],
            Self::Lima => vec![false, true, false, false],
            Self::Mike => vec![true, true],
            Self::November => vec![true, false],
            Self::Oscar => vec![true, true, true],
            Self::Papa => vec![false, true, true, false],
            Self::Quebec => vec![true, true, false, true],
            Self::Romeo => vec![false, true, false],
            Self::Sierra => vec![false, false, false],
            Self::Tango => vec![true],
            Self::Uniform => vec![false, false, true],
            Self::Victor => vec![false, false, false, true],
            Self::Whiskey => vec![false, true, true],
            Self::Xray => vec![true, false, false, true],
            Self::Yankee => vec![true, false, true, true],
            Self::Zulu => vec![true, true, false, false],
        }
    }

    fn phonetic(&self) -> &str {
        match &self {
            Self::Alfa => "al-fah",
            Self::Bravo => "bruh-voh",
            Self::Charlie => "char-lee",
            Self::Delta => "dell-tah",
            Self::Echo => "eck-oh",
            Self::Foxtrot => "foks-trot",
            Self::Golf => "golf",
            Self::Hotel => "hoh-tel",
            Self::India => "in-dee-ah",
            Self::Juliett => "jew-lee-ett",
            Self::Kilo => "key-loh",
            Self::Lima => "lee-mah",
            Self::Mike => "mike",
            Self::November => "no-vem-ber",
            Self::Oscar => "oss-cah",
            Self::Papa => "pah-pah",
            Self::Quebec => "keh-beck",
            Self::Romeo => "row-me-oh",
            Self::Sierra => "see-air-rah",
            Self::Tango => "tang-go",
            Self::Uniform => "you-nee-form",
            Self::Victor => "vic-tah",
            Self::Whiskey => "wiss-key",
            Self::Xray => "ecks-ray",
            Self::Yankee => "yang-key",
            Self::Zulu => "zoo-loo",
        }
    }

    fn semaphore(&self) -> SemaphorePositions {
        match self {
            // Grouped by `left`.
            Self::Alfa => SemaphorePositions { left: 4, right: 5 },
            Self::Bravo => SemaphorePositions { left: 4, right: 6 },
            Self::Charlie => SemaphorePositions { left: 4, right: 7 },
            Self::Delta => SemaphorePositions { left: 4, right: 0 },

            // Grouped by `right`.
            Self::Echo => SemaphorePositions { left: 1, right: 4 },
            Self::Foxtrot => SemaphorePositions { left: 2, right: 4 },
            Self::Golf => SemaphorePositions { left: 3, right: 4 },

            // Grouped by `left`.
            Self::Hotel => SemaphorePositions { left: 5, right: 6 },
            Self::India => SemaphorePositions { left: 5, right: 7 },

            Self::Juliett => SemaphorePositions { left: 2, right: 0 },

            // Grouped by `right`.
            Self::Kilo => SemaphorePositions { left: 0, right: 5 },
            Self::Lima => SemaphorePositions { left: 1, right: 5 },
            Self::Mike => SemaphorePositions { left: 2, right: 5 },
            Self::November => SemaphorePositions { left: 3, right: 5 },

            // Grouped by `right`.
            Self::Oscar => SemaphorePositions { left: 7, right: 6 },
            Self::Papa => SemaphorePositions { left: 0, right: 6 },
            Self::Quebec => SemaphorePositions { left: 1, right: 6 },
            Self::Romeo => SemaphorePositions { left: 2, right: 6 },
            Self::Sierra => SemaphorePositions { left: 3, right: 6 },

            // Grouped by `right`.
            Self::Tango => SemaphorePositions { left: 0, right: 7 },
            Self::Uniform => SemaphorePositions { left: 1, right: 7 },

            Self::Victor => SemaphorePositions { left: 3, right: 0 },

            // Grouped by `right`.
            Self::Whiskey => SemaphorePositions { left: 2, right: 1 },
            Self::Xray => SemaphorePositions { left: 3, right: 1 },

            // Grouped by `left`.
            Self::Yankee => SemaphorePositions { left: 2, right: 7 },
            Self::Zulu => SemaphorePositions { left: 2, right: 3 },
        }
    }
}

impl fmt::Display for NatoAlphabetLetter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Self::Alfa => "Alfa",
            Self::Bravo => "Bravo",
            Self::Charlie => "Charlie",
            Self::Delta => "Delta",
            Self::Echo => "Echo",
            Self::Foxtrot => "Foxtrot",
            Self::Golf => "Golf",
            Self::Hotel => "Hotel",
            Self::India => "India",
            Self::Juliett => "Juliett",
            Self::Kilo => "Kilo",
            Self::Lima => "Lima",
            Self::Mike => "Mike",
            Self::November => "November",
            Self::Oscar => "Oscar",
            Self::Papa => "Papa",
            Self::Quebec => "Quebec",
            Self::Romeo => "Romeo",
            Self::Sierra => "Sierra",
            Self::Tango => "Tango",
            Self::Uniform => "Uniform",
            Self::Victor => "Victor",
            Self::Whiskey => "Whiskey",
            Self::Xray => "Xray",
            Self::Yankee => "Yankee",
            Self::Zulu => "Zulu",
        };

        write!(f, "{}", s)
    }
}

/// NATO-standardized numbers.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum NatoNumber {
    /// (_wun_)
    ///
    /// Morse code: `.----`
    One,
    /// (_too_)
    ///
    /// Morse code: `..---`
    Two,
    /// (_tree_)
    ///
    /// Morse code: `...--`
    Three,
    /// (_fow-er_)
    ///
    /// Morse code: `....-`
    Four,
    /// (_fife_)
    ///
    /// Morse code: `.....`
    Five,
    /// (_six_)
    ///
    /// Morse code: `-....`
    Six,
    /// (_sev-en_)
    ///
    /// Morse code: `--...`
    Seven,
    /// (_ait_)
    ///
    /// Morse code: `---..`
    Eight,
    /// (_nin-er_)
    ///
    /// Morse code: `----.`
    Nine,
    /// (_zee-ro_)
    ///
    /// Morse code: `-----`
    Zero,
}

impl NatoCode for NatoNumber {
    fn morse(&self) -> Vec<bool> {
        match self {
            Self::One => vec![false, true, true, true, true],
            Self::Two => vec![false, false, true, true, true],
            Self::Three => vec![false, false, false, true, true],
            Self::Four => vec![false, false, false, false, true],
            Self::Five => vec![false, false, false, false, false],
            Self::Six => vec![true, false, false, false, false],
            Self::Seven => vec![true, true, false, false, false],
            Self::Eight => vec![true, true, true, false, false],
            Self::Nine => vec![true, true, true, true, false],
            Self::Zero => vec![true, true, true, true, true],
        }
    }

    fn phonetic(&self) -> &str {
        match self {
            Self::One => "wun",
            Self::Two => "too",
            Self::Three => "tree",
            Self::Four => "fow-er",
            Self::Five => "fife",
            Self::Six => "six",
            Self::Seven => "sev-en",
            Self::Eight => "ait",
            Self::Nine => "nin-er",
            Self::Zero => "zee-ro",
        }
    }

    fn semaphore(&self) -> SemaphorePositions {
        match self {
            Self::One => SemaphorePositions { left: 4, right: 5 },
            Self::Two => SemaphorePositions { left: 4, right: 6 },
            Self::Three => SemaphorePositions { left: 4, right: 7 },
            Self::Four => SemaphorePositions { left: 4, right: 0 },
            Self::Five => SemaphorePositions { left: 1, right: 4 },
            Self::Six => SemaphorePositions { left: 2, right: 4 },
            Self::Seven => SemaphorePositions { left: 3, right: 4 },
            Self::Eight => SemaphorePositions { left: 5, right: 6 },
            Self::Nine => SemaphorePositions { left: 5, right: 0 },
            Self::Zero => SemaphorePositions { left: 0, right: 5 },
        }
    }
}

impl fmt::Display for NatoNumber {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Self::One => "One",
            Self::Two => "Two",
            Self::Three => "Three",
            Self::Four => "Four",
            Self::Five => "Five",
            Self::Six => "Six",
            Self::Seven => "Seven",
            Self::Eight => "Eight",
            Self::Nine => "Nine",
            Self::Zero => "Zero",
        };

        write!(f, "{}", s)
    }
}

pub const NATO_ALPHABET: [NatoAlphabetLetter; 26] = [
    NatoAlphabetLetter::Alfa,
    NatoAlphabetLetter::Bravo,
    NatoAlphabetLetter::Charlie,
    NatoAlphabetLetter::Delta,
    NatoAlphabetLetter::Echo,
    NatoAlphabetLetter::Foxtrot,
    NatoAlphabetLetter::Golf,
    NatoAlphabetLetter::Hotel,
    NatoAlphabetLetter::India,
    NatoAlphabetLetter::Juliett,
    NatoAlphabetLetter::Kilo,
    NatoAlphabetLetter::Lima,
    NatoAlphabetLetter::Mike,
    NatoAlphabetLetter::November,
    NatoAlphabetLetter::Oscar,
    NatoAlphabetLetter::Papa,
    NatoAlphabetLetter::Quebec,
    NatoAlphabetLetter::Romeo,
    NatoAlphabetLetter::Sierra,
    NatoAlphabetLetter::Tango,
    NatoAlphabetLetter::Uniform,
    NatoAlphabetLetter::Victor,
    NatoAlphabetLetter::Whiskey,
    NatoAlphabetLetter::Xray,
    NatoAlphabetLetter::Yankee,
    NatoAlphabetLetter::Zulu,
];

#[cfg(test)]
mod alphabet_tests {
    use super::{NatoAlphabetLetter, NatoCode, SemaphorePositions};

    #[test]
    fn test_morse() {
        for (input, expected) in [
            (NatoAlphabetLetter::Alfa, vec![false, true]),
            (NatoAlphabetLetter::Bravo, vec![true, false, false, false]),
            (NatoAlphabetLetter::Charlie, vec![true, false, true, false]),
            (NatoAlphabetLetter::Delta, vec![true, false, false]),
            (NatoAlphabetLetter::Echo, vec![false]),
            (NatoAlphabetLetter::Foxtrot, vec![false, false, true, false]),
            (NatoAlphabetLetter::Golf, vec![true, true, false]),
            (NatoAlphabetLetter::Hotel, vec![false, false, false, false]),
            (NatoAlphabetLetter::India, vec![false, false]),
            (NatoAlphabetLetter::Juliett, vec![false, true, true, true]),
            (NatoAlphabetLetter::Kilo, vec![true, false, true]),
            (NatoAlphabetLetter::Lima, vec![false, true, false, false]),
            (NatoAlphabetLetter::Mike, vec![true, true]),
            (NatoAlphabetLetter::November, vec![true, false]),
            (NatoAlphabetLetter::Oscar, vec![true, true, true]),
            (NatoAlphabetLetter::Papa, vec![false, true, true, false]),
            (NatoAlphabetLetter::Quebec, vec![true, true, false, true]),
            (NatoAlphabetLetter::Romeo, vec![false, true, false]),
            (NatoAlphabetLetter::Sierra, vec![false, false, false]),
            (NatoAlphabetLetter::Tango, vec![true]),
            (NatoAlphabetLetter::Uniform, vec![false, false, true]),
            (NatoAlphabetLetter::Victor, vec![false, false, false, true]),
            (NatoAlphabetLetter::Whiskey, vec![false, true, true]),
            (NatoAlphabetLetter::Xray, vec![true, false, false, true]),
            (NatoAlphabetLetter::Yankee, vec![true, false, true, true]),
            (NatoAlphabetLetter::Zulu, vec![true, true, false, false]),
        ] {
            assert_eq!(input.morse(), expected);
        }
    }

    #[test]
    fn test_phonetic() {
        for (input, expected) in [
            (NatoAlphabetLetter::Alfa, "al-fah"),
            (NatoAlphabetLetter::Bravo, "bruh-voh"),
            (NatoAlphabetLetter::Charlie, "char-lee"),
            (NatoAlphabetLetter::Delta, "dell-tah"),
            (NatoAlphabetLetter::Echo, "eck-oh"),
            (NatoAlphabetLetter::Foxtrot, "foks-trot"),
            (NatoAlphabetLetter::Golf, "golf"),
            (NatoAlphabetLetter::Hotel, "hoh-tel"),
            (NatoAlphabetLetter::India, "in-dee-ah"),
            (NatoAlphabetLetter::Juliett, "jew-lee-ett"),
            (NatoAlphabetLetter::Kilo, "key-loh"),
            (NatoAlphabetLetter::Lima, "lee-mah"),
            (NatoAlphabetLetter::Mike, "mike"),
            (NatoAlphabetLetter::November, "no-vem-ber"),
            (NatoAlphabetLetter::Oscar, "oss-cah"),
            (NatoAlphabetLetter::Papa, "pah-pah"),
            (NatoAlphabetLetter::Quebec, "keh-beck"),
            (NatoAlphabetLetter::Romeo, "row-me-oh"),
            (NatoAlphabetLetter::Sierra, "see-air-rah"),
            (NatoAlphabetLetter::Tango, "tang-go"),
            (NatoAlphabetLetter::Uniform, "you-nee-form"),
            (NatoAlphabetLetter::Victor, "vic-tah"),
            (NatoAlphabetLetter::Whiskey, "wiss-key"),
            (NatoAlphabetLetter::Xray, "ecks-ray"),
            (NatoAlphabetLetter::Yankee, "yang-key"),
            (NatoAlphabetLetter::Zulu, "zoo-loo"),
        ] {
            assert_eq!(input.phonetic(), expected);
        }
    }

    #[test]
    fn test_semaphore() {
        for (input, expected) in [
            (
                NatoAlphabetLetter::Alfa,
                SemaphorePositions { left: 4, right: 5 },
            ),
            (
                NatoAlphabetLetter::Bravo,
                SemaphorePositions { left: 4, right: 6 },
            ),
            (
                NatoAlphabetLetter::Charlie,
                SemaphorePositions { left: 4, right: 7 },
            ),
            (
                NatoAlphabetLetter::Delta,
                SemaphorePositions { left: 4, right: 0 },
            ),
            (
                NatoAlphabetLetter::Echo,
                SemaphorePositions { left: 1, right: 4 },
            ),
            (
                NatoAlphabetLetter::Foxtrot,
                SemaphorePositions { left: 2, right: 4 },
            ),
            (
                NatoAlphabetLetter::Golf,
                SemaphorePositions { left: 3, right: 4 },
            ),
            (
                NatoAlphabetLetter::Hotel,
                SemaphorePositions { left: 5, right: 6 },
            ),
            (
                NatoAlphabetLetter::India,
                SemaphorePositions { left: 5, right: 7 },
            ),
            (
                NatoAlphabetLetter::Juliett,
                SemaphorePositions { left: 2, right: 0 },
            ),
            (
                NatoAlphabetLetter::Kilo,
                SemaphorePositions { left: 0, right: 5 },
            ),
            (
                NatoAlphabetLetter::Lima,
                SemaphorePositions { left: 1, right: 5 },
            ),
            (
                NatoAlphabetLetter::Mike,
                SemaphorePositions { left: 2, right: 5 },
            ),
            (
                NatoAlphabetLetter::November,
                SemaphorePositions { left: 3, right: 5 },
            ),
            (
                NatoAlphabetLetter::Oscar,
                SemaphorePositions { left: 7, right: 6 },
            ),
            (
                NatoAlphabetLetter::Papa,
                SemaphorePositions { left: 0, right: 6 },
            ),
            (
                NatoAlphabetLetter::Quebec,
                SemaphorePositions { left: 1, right: 6 },
            ),
            (
                NatoAlphabetLetter::Romeo,
                SemaphorePositions { left: 2, right: 6 },
            ),
            (
                NatoAlphabetLetter::Sierra,
                SemaphorePositions { left: 3, right: 6 },
            ),
            (
                NatoAlphabetLetter::Tango,
                SemaphorePositions { left: 0, right: 7 },
            ),
            (
                NatoAlphabetLetter::Uniform,
                SemaphorePositions { left: 1, right: 7 },
            ),
            (
                NatoAlphabetLetter::Victor,
                SemaphorePositions { left: 3, right: 0 },
            ),
            (
                NatoAlphabetLetter::Whiskey,
                SemaphorePositions { left: 2, right: 1 },
            ),
            (
                NatoAlphabetLetter::Xray,
                SemaphorePositions { left: 3, right: 1 },
            ),
            (
                NatoAlphabetLetter::Yankee,
                SemaphorePositions { left: 2, right: 7 },
            ),
            (
                NatoAlphabetLetter::Zulu,
                SemaphorePositions { left: 2, right: 3 },
            ),
        ] {
            assert_eq!(input.semaphore(), expected);
        }
    }

    #[test]
    fn test_to_string() {
        for (input, expected) in [
            (NatoAlphabetLetter::Alfa, "Alfa"),
            (NatoAlphabetLetter::Bravo, "Bravo"),
            (NatoAlphabetLetter::Charlie, "Charlie"),
            (NatoAlphabetLetter::Delta, "Delta"),
            (NatoAlphabetLetter::Echo, "Echo"),
            (NatoAlphabetLetter::Foxtrot, "Foxtrot"),
            (NatoAlphabetLetter::Golf, "Golf"),
            (NatoAlphabetLetter::Hotel, "Hotel"),
            (NatoAlphabetLetter::India, "India"),
            (NatoAlphabetLetter::Juliett, "Juliett"),
            (NatoAlphabetLetter::Kilo, "Kilo"),
            (NatoAlphabetLetter::Lima, "Lima"),
            (NatoAlphabetLetter::Mike, "Mike"),
            (NatoAlphabetLetter::November, "November"),
            (NatoAlphabetLetter::Oscar, "Oscar"),
            (NatoAlphabetLetter::Papa, "Papa"),
            (NatoAlphabetLetter::Quebec, "Quebec"),
            (NatoAlphabetLetter::Romeo, "Romeo"),
            (NatoAlphabetLetter::Sierra, "Sierra"),
            (NatoAlphabetLetter::Tango, "Tango"),
            (NatoAlphabetLetter::Uniform, "Uniform"),
            (NatoAlphabetLetter::Victor, "Victor"),
            (NatoAlphabetLetter::Whiskey, "Whiskey"),
            (NatoAlphabetLetter::Xray, "Xray"),
            (NatoAlphabetLetter::Yankee, "Yankee"),
            (NatoAlphabetLetter::Zulu, "Zulu"),
        ] {
            assert_eq!(input.to_string(), expected);
        }
    }
}

#[cfg(test)]
mod numbers_tests {
    use super::{NatoCode, NatoNumber, SemaphorePositions};

    #[test]
    fn test_morse() {
        for (input, expected) in [
            (NatoNumber::One, vec![false, true, true, true, true]),
            (NatoNumber::Two, vec![false, false, true, true, true]),
            (NatoNumber::Three, vec![false, false, false, true, true]),
            (NatoNumber::Four, vec![false, false, false, false, true]),
            (NatoNumber::Five, vec![false, false, false, false, false]),
            (NatoNumber::Six, vec![true, false, false, false, false]),
            (NatoNumber::Seven, vec![true, true, false, false, false]),
            (NatoNumber::Eight, vec![true, true, true, false, false]),
            (NatoNumber::Nine, vec![true, true, true, true, false]),
            (NatoNumber::Zero, vec![true, true, true, true, true]),
        ] {
            assert_eq!(input.morse(), expected);
        }
    }

    #[test]
    fn test_phonetic() {
        for (input, expected) in [
            (NatoNumber::One, "wun"),
            (NatoNumber::Two, "too"),
            (NatoNumber::Three, "tree"),
            (NatoNumber::Four, "fow-er"),
            (NatoNumber::Five, "fife"),
            (NatoNumber::Six, "six"),
            (NatoNumber::Seven, "sev-en"),
            (NatoNumber::Eight, "ait"),
            (NatoNumber::Nine, "nin-er"),
            (NatoNumber::Zero, "zee-ro"),
        ] {
            assert_eq!(input.phonetic(), expected);
        }
    }

    #[test]
    fn test_semaphore() {
        for (input, expected) in [
            (NatoNumber::One, SemaphorePositions { left: 4, right: 5 }),
            (NatoNumber::Two, SemaphorePositions { left: 4, right: 6 }),
            (NatoNumber::Three, SemaphorePositions { left: 4, right: 7 }),
            (NatoNumber::Four, SemaphorePositions { left: 4, right: 0 }),
            (NatoNumber::Five, SemaphorePositions { left: 1, right: 4 }),
            (NatoNumber::Six, SemaphorePositions { left: 2, right: 4 }),
            (NatoNumber::Seven, SemaphorePositions { left: 3, right: 4 }),
            (NatoNumber::Eight, SemaphorePositions { left: 5, right: 6 }),
            (NatoNumber::Nine, SemaphorePositions { left: 5, right: 0 }),
            (NatoNumber::Zero, SemaphorePositions { left: 0, right: 5 }),
        ] {
            assert_eq!(input.semaphore(), expected);
        }
    }

    #[test]
    fn test_to_string() {
        for (input, expected) in [
            (NatoNumber::One, "One"),
            (NatoNumber::Two, "Two"),
            (NatoNumber::Three, "Three"),
            (NatoNumber::Four, "Four"),
            (NatoNumber::Five, "Five"),
            (NatoNumber::Six, "Six"),
            (NatoNumber::Seven, "Seven"),
            (NatoNumber::Eight, "Eight"),
            (NatoNumber::Nine, "Nine"),
            (NatoNumber::Zero, "Zero"),
        ] {
            assert_eq!(input.to_string(), expected);
        }
    }
}

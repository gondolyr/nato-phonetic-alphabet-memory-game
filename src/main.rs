use std::collections::HashSet;
use std::time::Instant;
use std::{env, io};

use nato_phonetic_alphabet_memory_game::NATO_ALPHABET;
use rand::seq::SliceRandom;
use rand::thread_rng;

fn main() {
    let mut args = env::args();
    // Skip the program name.
    args.next();

    let infinite = match args.next() {
        Some(arg) => arg.to_lowercase() == "infinite",
        None => false,
    };

    let mut rng = thread_rng();

    let mut guess = String::new();

    println!(
        "Guess the phonetic word! ({} mode)",
        if infinite { "Infinite" } else { "Quiz" }
    );

    let mut nato_letter = NATO_ALPHABET.choose(&mut rng).unwrap();
    let mut chosen_letters = HashSet::new();

    // Ensure that the same letter doesn't appear twice for infinite mode.
    let mut previous_letter = nato_letter.clone();

    let start = Instant::now();
    while chosen_letters.len() < NATO_ALPHABET.len() {
        if infinite {
            while &previous_letter == nato_letter {
                nato_letter = NATO_ALPHABET.choose(&mut rng).unwrap();
            }
        } else {
            while chosen_letters.contains(nato_letter) {
                nato_letter = NATO_ALPHABET.choose(&mut rng).unwrap();
            }
        }
        let first_letter = &nato_letter.to_string()[0..1];

        println!("Please input your guess for '{}'.", first_letter);

        io::stdin()
            .read_line(&mut guess)
            .expect("failed to read line");
        // Remove the extraneous whitespace characters including the newline character.
        guess = guess.trim().to_lowercase();

        if guess == nato_letter.to_string().to_lowercase() {
            println!("Correct!\n");

            if !infinite {
                chosen_letters.insert(nato_letter.clone());
            }
        } else {
            println!("Sorry, the correct answer was: {}\n", nato_letter);

            if !infinite {
                break;
            }
        }

        if infinite {
            previous_letter = nato_letter.clone();
        }

        // Reset the guess for the next loop.
        guess = "".to_string();
    }
    let duration = start.elapsed();

    println!(
        "Your score was {} and you took {} seconds to finish.",
        chosen_letters.len(),
        duration.as_secs()
    );
}

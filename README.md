# NATO Phonetic Alphabet Memory Game

CLI game to practice memorizing the [NATO phonetic alphabet][nato-phonetic-alphabet-wiki] ([PDF][nato-phonetic-alphabet-pdf]).
A [copy of the PDF](docs/20180111_nato-alphabet-sign-signal.pdf) can be found in the [docs folder](docs/).

[nato-phonetic-alphabet-pdf]: https://www.nato.int/nato_static_fl2014/assets/pdf/pdf_2018_01/20180111_nato-alphabet-sign-signal.pdf
[nato-phonetic-alphabet-wiki]: https://en.wikipedia.org/wiki/NATO_phonetic_alphabet

# Usage

```sh
$ nato-phonetic-alphabet-memory-game [<infinite>]
```

## Description

Inputs are case-insensitive.

In "quiz" mode, which is the default, the game will randomly select unique letters until the entire alphabet has been exhausted.

In "infinite" mode, the game will randomly select letters but will not end until the user terminates the game using `Ctrl-c`.

## Args

`<infinite>`

If the exact word "infinite" is provided here, the game will loop endlessly for practice.
Excluding this will default to the quiz mode, which will randomly go through each letter until each one has been chosen.
